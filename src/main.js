import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import Vuelidate from 'vuelidate'
import router from './router'
import store from './store'


Vue.use(Vuelidate); // 3er party library to validate form inputs

// to use AXIOS you dont need to set vue.use()
axios.defaults.baseURL = 'https://vue-axios-cf3f6.firebaseio.com/'
 // axios.defaults.headers.common['Authorization'] = 'fasfdsa' // dummy token as a test
axios.defaults.headers.get['Accepts'] = 'application/json'

const reqInterceptor = axios.interceptors.request.use(config => { // request interceptors = we must return it to allow the component to REQUEST the data expected
  console.log('Request Interceptor', config)
  return config
})
const resInterceptor = axios.interceptors.response.use(res => { // Response interceptors = we must return it to allow the component to GET the data expected
  alert('Response Interceptor', res)
  return res
})

axios.interceptors.request.eject(reqInterceptor) // .eject()//  dosent allow to see the response (console.log) of the constant
axios.interceptors.response.eject(resInterceptor)

new Vue({
  el: '#app',
  router,
  store, // using it you can use store == this.$store in components
  render: h => h(App)
})
