import axios from 'axios'

// Instance to set a new URL for a specific case: it's setup globally = it can be used for any component
const instance = axios.create({
  baseURL: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty'
})

// instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance